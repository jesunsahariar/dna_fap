#include <stdio.h>
#include<iostream>
#include <stdlib.h>
#include <math.h>
#include<vector>
#include<algorithm>
#include<fstream>
#include <time.h>
using namespace std;


#define NP 256
#define FoodNumber NP/2 
#define limit 40 
#define maxCycle 3000

int no_of_fragments=177;
#define D 177
#define runtime 2
char filename[200]="m15421_454_output.txt";


int random_n=1;
vector < vector <int> > Foods;
vector <int > solution;
unsigned int fitness[FoodNumber];  
int trial[FoodNumber]; 
double prob[FoodNumber]; 
int contiglenth[FoodNumber];
int contig=0;
unsigned int FitnessSol; 
unsigned int  GlobalMax;
int Mincontig;
unsigned int Mincontigindex;
int GlobalMaxIndex;
vector<vector<int> > GlobalParams; 
unsigned int  GlobalMaxs[maxCycle]; 
double  cycleruntime[maxCycle];

double r; 

int flag=1;
int fitarray[1060][1060];
//////////////////////////////////////////////////PALs//////////////////
int diffcontig=0;
int difffitness=0;
int cutoff=4;
void CalculaeDelta(vector<int>s, int i, int j);
struct palsdata{
       int i;
       int j;
       int diffcontig;
       int difffitness;
};

//PALs////////////////////////////////////////////////
vector<int> PALs(vector<int> singlesol, int version)
{
	int i,j,k;
	//vector<palsdata> L;
	//L.clear();
	
	difffitness=0;
	diffcontig=0;
	//cout<<"L Size"<<L.size()<<endl;
	int bestindexi=-1,bestindexj=-1,lesscontig, bestfitness;
	int flag=0;
	for(i=1;i<no_of_fragments-1;i++)
	{
		//cout<<"Turn : "<<i<<endl;
		for(j=i;j<no_of_fragments-1;j++)
		{
			CalculaeDelta(singlesol, i, j);
			if(diffcontig<=0)
			{
			        bestindexi=i;
			        bestindexj=j;
			        bestfitness=difffitness;
			        flag=1;
			        break;
			}
			
		}
		if(flag==1) break;
	}
	//cout<<"Here";
	//cout<<bestindexi<<"    "<<bestindexj<<"  "<<bestfitness<<endl;
	if(bestindexi!=-1)
	{
		reverse(singlesol.begin()+bestindexi, singlesol.begin()+bestindexj+1); 
		
	}
	return singlesol;
}
vector<int> PALs(vector<int> singlesol)
{
	int i,j,k;
	//vector<palsdata> L;
	//L.clear();
	
	difffitness=0;
	diffcontig=0;
	//cout<<"L Size"<<L.size()<<endl;
	int bestindexi=-1,bestindexj=-1,lesscontig, bestfitness;
	int flag=0;
	for(i=1;i<no_of_fragments-1;i++)
	{
		//cout<<"Turn : "<<i<<endl;
		for(j=i;j<no_of_fragments-1;j++)
		{
			CalculaeDelta(singlesol, i, j);
			if(diffcontig<=0 && difffitness>cutoff)
			{
			        bestindexi=i;
			        bestindexj=j;
			        bestfitness=difffitness;
			        flag=1;
			        break;
			}
			
		}
		if(flag==1) break;
	}
	//cout<<"Here";
	//cout<<bestindexi<<"    "<<bestindexj<<"  "<<bestfitness<<endl;
	if(bestindexi!=-1)
	{
		reverse(singlesol.begin()+bestindexi, singlesol.begin()+bestindexj+1); 
		
	}
	return singlesol;
}
void CalculaeDelta(vector<int> s, int i, int j)
{

	diffcontig=0;
	difffitness=0;

	difffitness = fitarray[s[i-1]][s[j]] + fitarray[s[i]][s[j+1]]; 
	difffitness = difffitness-fitarray[s[i-1]][s[i]]-fitarray[s[j]][s[j+1]] ;
	
	
	if (fitarray[s[i-1]][s[i]] > cutoff)
	{
		diffcontig = diffcontig + 1;
	}
	if(fitarray[s[j]][s[j+1]] > cutoff)
	{
		diffcontig = diffcontig + 1;
	}

	if(fitarray[s[i-1]][s[j]] > cutoff)
	{
		diffcontig = diffcontig-1;
	}

	if(fitarray[s[i]][s[j+1]] > cutoff)
	{
		diffcontig = diffcontig-1;
	}
}
///////////////////////////////////////////////////PALs////////////////////

///////////////read fitness///////////////////////
unsigned int CalculateFitness(vector <int> _sol)
{
	unsigned int fit=0,i,j;
    	ifstream fin(filename);
    	if(flag==1)
    	{
			for(i=1;i<=D;i++)//change to D
			{
				  for(j=1;j<=D;j++)//change to D
				  {
					  fin>>fitarray[i][j];
					  if(fitarray[i][j]<0) fitarray[i][j]=-fitarray[i][j];
				  }
			}
			fin.close();
			flag=0;
			
        }
        //cout<<"begin calc fitness"<<endl;
		contig=0;
		for(i=1; i<_sol.size(); i++)
        {
				//cout<<endl<<"got "<<i<<"   "<<_sol[i-1]<<"  "<<"  "<<_sol[i]<<"   "<<fitarray[_sol[i-1]][_sol[i]];
				fit+=fitarray[_sol[i-1]][_sol[i]];
				if(fitarray[_sol[i-1]][_sol[i]]<cutoff) contig=contig+1;
        }
        //cout<<"end calc fitness"<<endl;
        return fit;

}
///////////////read fitness///////////////////////
////initialization////////////////////////////////
ptrdiff_t myrandom (ptrdiff_t i) { 
	return rand()%i;
}

// pointer object to it:
ptrdiff_t (*p_myrandom)(ptrdiff_t) = myrandom;

void init(int index,int op)
{
        vector<int> sol;
        vector<int>::iterator it;
        sol.clear();
        
        for(int i=1;i<=D;i++)
        {
        	sol.push_back(i);
        }
        random_shuffle ( sol.begin(), sol.end());
        random_shuffle ( sol.begin(), sol.end(),p_myrandom );
        if(op==1) Foods.push_back(sol);
        else {Foods[index]=sol;}
        fitness[index]=CalculateFitness(Foods[index]);
		trial[index]=0;
		contiglenth[index]=contig;
	//for (it=sol.begin(); it!=sol.end(); ++it)
    			//cout << " " << *it;
    	//cout<<" and fitness ="<<fitness[index]<<endl;
}

/*All food sources are initialized */
void initial()
{
	int i;
	//srand(time(NULL));
	Foods.clear();
	for(i=0;i<FoodNumber;i++)
	{
		init(i, 1);
	}
	//cout<<"end iteration"<<endl;
	GlobalMax=fitness[0];
}

//////////////////////////////////////////////////

//////////////ABC steps//////////////////////////
void SendEmployedBees()
{
  	int i,j;
  
   	for (i=0;i<FoodNumber;i++)
        {
        	
                vector<int> temp;
                temp.clear();
                temp=Foods[i];
                //cout<<" fitness Before :  "<<fitness[i]<<endl;
                temp=PALs(temp);
                
                FitnessSol=fitness[i]+difffitness;
                //cout<<"fitness after pals"<<FitnessSol<<endl;
        
        	/*a greedy selection is applied between the current solution i and its mutant*/
				if (FitnessSol>fitness[i])
				{
					
					trial[i]=0;
					Foods[i]=temp;
					fitness[i]=FitnessSol;
					contiglenth[i]=contiglenth[i]+diffcontig;
				}
				else
				{   
					trial[i]=trial[i]+1;
				}


        }

        /*end of employed bee phase*/
}
void CalculateProbabilities()
{
     	double div=GlobalMax+(GlobalMax*0.5);
		for (int i=0;i<FoodNumber;i++)
        {
           	//cout<<fitness[i]<<"     "<<div<<endl;
			prob[i]=(fitness[i]/div);
			//cout<<prob[i]<<endl;
        }

}
void SendOnlookerBees()
{

  	int i,j,t;
  	i=0;
  	t=0;
  	
  	while(t<FoodNumber)
	{

		r = (   (double)rand() / ((double)(RAND_MAX)+(double)(1)) );
		//cout<<"r ="<<r<<endl;
		if(prob[i]>r) 
		{        
			t++;
			vector<int> temp;
			//cout<<"inserted for foodsource"<<i<<endl;
			temp.clear();
			temp=Foods[i];
			temp=PALs(temp);
			FitnessSol=fitness[i]+difffitness;
			
			if (FitnessSol>fitness[i])
			{
				
				trial[i]=0;
				Foods[i]=temp;
				fitness[i]=FitnessSol;
				contiglenth[i]=contiglenth[i]+diffcontig;
			}
			else
			{   
				trial[i]=trial[i]+1;
			}
		} 
		i++;
		if (i==FoodNumber-1){i=0;}
        
	}
}

void SendScoutBees()
{
	int maxtrialindex,i;
	maxtrialindex=0;
	for (i=1;i<FoodNumber;i++)
        {
         	if (trial[i]>trial[maxtrialindex])
         			maxtrialindex=i;
        }
	if(trial[maxtrialindex]>=limit)
	{     
		
		{
			
			random_shuffle(Foods[maxtrialindex].begin(), Foods[maxtrialindex].end());
			fitness[maxtrialindex]=CalculateFitness(Foods[maxtrialindex]);
			trial[maxtrialindex]=0;
			contiglenth[maxtrialindex]=contig;
		}
		
	}
	
}

////////////////////////////////////////////////
////memo best source //////////////////////////

void MemorizeBestSource()
{
   	int i,j;
    	GlobalMax=fitness[0];
	GlobalMaxIndex=0;
	Mincontig=contiglenth[0];
	Mincontigindex=0;
	//cout<<"initial Mincontig :"<<Mincontig;
	for(i=0;i<FoodNumber;i++)
	{
		if (fitness[i]>GlobalMax)
		{
        		GlobalMax=fitness[i];
				GlobalMaxIndex=i;
		}
		if(contiglenth[i]<Mincontig)
		{
			Mincontig=contiglenth[i];
			//cout<<"changing min contig :"<<Mincontig<<endl;
			Mincontigindex=i;
		}
           		
	}
 }

//////////////////////////////////////////////////
int main()
{

	int iter,run,j;
	double diff;
	time_t start, end;
	double mean;
	mean=0;
	srand(time(NULL));
	//freopen("m_5.txt", "w", stdout);

	for(run=0;run<runtime;run++)
	{

		initial();
		MemorizeBestSource();
		for (iter=0;iter<maxCycle;iter++)
    		{
    			if(iter==0) time(&start);
			SendEmployedBees();
			//cout<<"here1"; 
			MemorizeBestSource();
			//cout<<"here2";
    			CalculateProbabilities();
				//cout<<"here3";
    			SendOnlookerBees();
				//cout<<"here4";
    			MemorizeBestSource();
				//cout<<"here5";
    			SendScoutBees();
				//cout<<"here6";
    			MemorizeBestSource();
				//cout<<"here7";
    			//printf("%d. iter: %d \n",iter+1,GlobalMax);
    			cout<<iter<<"   "<<GlobalMax<<"    "<<contiglenth[GlobalMaxIndex]<<endl;
				//GlobalMaxs[iter]=GlobalMax;
			cout<<iter<<"   "<<Mincontig<<"    "<<fitness[Mincontigindex]<<endl;
		        cout<<"-------------------------------"<<endl;
    			
    		}
		
		  
	}

	return 1;
}

