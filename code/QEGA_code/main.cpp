#include <stdio.h>
#include<iostream>
#include <stdlib.h>
#include <math.h>
#include<vector>
#include<algorithm>
#include<fstream>
#include <time.h>
#include<set>
using namespace std;


#define BEE 1024
#define FoodNumber BEE 
#define maxCycle 20000


#define D 177
#define runtime 1
int no_of_fragments=177;

vector < vector <int> > Foods;
unsigned int fitness[FoodNumber];  
 
int contiglenth[FoodNumber];
int contig=0;
unsigned int FitnessSol; 
unsigned int  GlobalMax;
int Mincontig;
unsigned int Mincontigindex;
int GlobalMaxIndex;
//int difffitness;
//int diffcontig;
unsigned int  GlobalMaxs[maxCycle]; 
double  cycleruntime[maxCycle];
double prob[FoodNumber];
double r; 

int flag=1;
int fitarray[1060][1060];
//////////////////////////////////////////////////PALs//////////////////
int diffcontig;
int difffitness;
int cutoff=30;
void CalculaeDelta(vector<int> s, int i, int j);

///////////////read fitness///////////////////////
unsigned int CalculateFitness(vector <int> _sol)
{
	unsigned int fit=0,i,j;
    	ifstream fin("m15421_454_output.txt");
    	if(flag==1)
    	{
			for(i=1;i<=D;i++)//change to D
			{
				  for(j=1;j<=D;j++)//change to D
				  {
					  fin>>fitarray[i][j];
					  if(fitarray[i][j]<0) fitarray[i][j]=-fitarray[i][j];
				  }
			}
			fin.close();
			flag=0;
			
        }
        //cout<<"begin calc fitness"<<endl;
	contig=0;
	for(unsigned int i=1; i<_sol.size(); i++)
        {
				//cout<<endl<<"got "<<i<<"   "<<_sol[i-1]<<"  "<<"  "<<_sol[i]<<"   "<<fitarray[_sol[i-1]][_sol[i]];
				fit+=fitarray[_sol[i-1]][_sol[i]];
				if(fitarray[_sol[i-1]][_sol[i]]<cutoff) contig=contig+1;
        }
        //cout<<"end calc fitness"<<endl;
        return fit;

}
///////////////read fitness///////////////////////
vector<int> PALs(vector<int> singlesol)
{
	int i,j,k;
	//vector<palsdata> L;
	//L.clear();
	
	
	//cout<<"L Size"<<L.size()<<endl;
	int bestindexi=-1,bestindexj=-1,lesscontig, bestfitness;
	int flag=0;
	for(i=1;i<no_of_fragments;i++)
	{
		//cout<<"Turn : "<<i<<endl;
		for(j=i;j<no_of_fragments;j++)
		{
			CalculaeDelta(singlesol, i, j);
			if(diffcontig<=0 && difffitness>cutoff)
			{
			        bestindexi=i;
			        bestindexj=j;
			        bestfitness=difffitness;
			        flag=1;
			        break;
			}
			
		}
		if(flag==1) break;
	}
	//cout<<"Here";
	//cout<<bestindexi<<"    "<<bestindexj<<"  "<<bestfitness<<endl;
	if(bestindexi!=-1)
	{
		reverse(singlesol.begin()+bestindexi, singlesol.begin()+bestindexj+1); 
		
	}
	return singlesol;
}
void CalculaeDelta(vector<int> s, int i, int j)
{

	diffcontig=0;
	difffitness=0;

	difffitness = fitarray[s[i-1]][s[j]] + fitarray[s[i]][s[j+1]]; 
	difffitness = difffitness-fitarray[s[i-1]][s[i]]-fitarray[s[j]][s[j+1]] ;
	
	
	if (fitarray[s[i-1]][s[i]] > cutoff)
	{
		diffcontig = diffcontig + 1;
	}
	if(fitarray[s[j]][s[j+1]] > cutoff)
	{
		diffcontig = diffcontig + 1;
	}

	if(fitarray[s[i-1]][s[j]] > cutoff)
	{
		diffcontig = diffcontig-1;
	}

	if(fitarray[s[i]][s[j+1]] > cutoff)
	{
		diffcontig = diffcontig-1;
	}
}

////initialization////////////////////////////////
ptrdiff_t myrandom (ptrdiff_t i) { 
	return rand()%i;
}

// pointer object to it:
ptrdiff_t (*p_myrandom)(ptrdiff_t) = myrandom;

void init(int index,int op)
{
        vector<int> sol;
        vector<int>::iterator it;
        sol.clear();
        
        for(int i=1;i<=D;i++)
        {
        	sol.push_back(i);
        }
        random_shuffle ( sol.begin(), sol.end());
        random_shuffle ( sol.begin(), sol.end(),p_myrandom );
        if(op==1) Foods.push_back(sol);
        else {Foods[index]=sol;}
        fitness[index]=CalculateFitness(Foods[index]);
		//trial[index]=0;
		contiglenth[index]=contig;
	/*cout<<endl<<"Before crossover"<<endl;
	for (it=sol.begin(); it!=sol.end(); ++it)
    			cout << " " << *it;
    	cout<<" and fitness ="<<fitness[index]<<endl;*/
}

/*All food sources are initialized */
void initial()
{
	int i;
	//srand(time(NULL));
	Foods.clear();
	for(i=0;i<FoodNumber;i++)
	{
		init(i, 1);
	}
	//cout<<"end iteration"<<endl;
	GlobalMax=fitness[0];
}

//////////////////////////////////////////////////

////memo best source //////////////////////////

void MemorizeBestSource()
{
   	int i,j;
    	GlobalMax=fitness[0];
	GlobalMaxIndex=0;
	Mincontig=contiglenth[0];
	Mincontigindex=0;
	//cout<<"initial Mincontig :"<<Mincontig;
	for(i=0;i<FoodNumber;i++)
	{
		if (fitness[i]>GlobalMax)
		{
        		GlobalMax=fitness[i];
			GlobalMaxIndex=i;
		}
		if(contiglenth[i]<Mincontig)
		{
			Mincontig=contiglenth[i];
			//cout<<"changing min contig :"<<Mincontig<<endl;
			Mincontigindex=i;
		}
           		
	}
 }
void CalculateProbabilities()
{
     	double div=GlobalMax+(GlobalMax*0.2);
		for (int i=0;i<FoodNumber;i++)
        {
           	//cout<<fitness[i]<<"     "<<div<<endl;
			prob[i]=(0.9)*(fitness[i]/div)+0.1;
			
        }

}
//////////////////////////////////////////////////
void orderedcrossover(int index,int version)
{
	int r1, r2,count=0;
	int maxindex,i;
	if(version==1) maxindex=GlobalMaxIndex;
	else maxindex=Mincontigindex;
	
	r1=rand()%D;
	r2=rand()%D;
	if(r1>r2)
	{
		int temp=r2;
		r2=r1;
		r1=temp;
	}
	set<int >r;
	vector<int >s;
	set<int >::iterator it;
	vector<int >::iterator it1;
	
	r.clear();
	for(int j=r1;j<=r2;j++)
	{
		r.insert(Foods[index][j]);
	}
	for(int k=0;k<D;k++)
	{
		if(r.count(Foods[maxindex][k])>0)
		{
		     s.push_back(Foods[maxindex][k]);
		     count++;
		}  
		if(count==r.size()) break;  
	}
	int j=r1;
	//cout<<"Set contains :"<<endl;
	for ( it1=s.begin() ; it1 != s.end(); it1++ )
	{
		Foods[index][j++]=*it1;
		//cout<<*it1;
	}
	//cout<<endl;
	fitness[index]=CalculateFitness(Foods[index]);
	contiglenth[index]=contig;
	/*cout<<"r1=  "<<r1<<"   r2="<<r2<<endl;
        for (it1=Foods[index].begin(); it1!=Foods[index].end(); ++it1)
    			cout << " " << *it1;
    	cout<<" and fitness ="<<fitness[index]<<endl;
    	cout<<endl;*/
}
void Breed(int version)
{
	//Breed upto best value i
	
	int no_bee_cross=(int)BEE/2;
	CalculateProbabilities();
	
	int t=0;
	while(t<no_bee_cross)
	{
		int r1=rand()%BEE;
		double r = ((double)rand() / ((double)(RAND_MAX)+(double)(1)) );
		if(r<prob[r1]){
			t++;
			orderedcrossover(r1, version);
		}
	}
	
}
/////////////////////////////////
void do_palsmutate()
{
	for(int i=0;i<FoodNumber;i++)
	{
		Foods[i]=PALs(Foods[i]);
		fitness[i]=fitness[i]+difffitness;
		contiglenth[i]=contiglenth[i]+diffcontig;
	}
	
}
/////////////////////////////////
int main()
{

	int iter,run,j;
	double mean;
	mean=0;
	srand(time(NULL));
	//freopen("output.txt", "w", stdout);

	for(run=0;run<runtime;run++)
	{

		initial();
		MemorizeBestSource();
		for (iter=0;iter<maxCycle;iter++)
    		{
    			
    			int r;
				r=rand()%3;
				if(r==0 || r==1 ) Breed(2);
				else Breed(1);
				do_palsmutate();
    			MemorizeBestSource();
    			cout<<iter<<"   "<<GlobalMax<<"    "<<contiglenth[GlobalMaxIndex]<<endl;
				cout<<iter<<"   "<<Mincontig<<"    "<<fitness[Mincontigindex]<<endl;
    			
    		}
		
		
	}

	return 1;
}

