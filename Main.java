package test;
/**
 *
 * @author ${user}
 */

import java.io.*;

class FileStreamsTest {

    String []s=new String[4000];
    int [][]fitarray=new int[2000][2000];
    int [][]status=new int[2000][2000];
    public int no_of_fragments=0;
    
    public int get_longestmatch(String s1,String s2)
    {
        int bestval=0;
        int len1=s1.length();
        
        if(s1.length()>=s2.length()){
            if (s1.contains(s2)){
                return s2.length();
            }
        }
        if(s2.length()>=s1.length()){
            if(s2.contains(s1)){
                return s1.length();
            }
        }
        
        for(int i=0;i<s2.length();i++)
        {
            String temp=s2.substring(0, (i+1));
            int position=s1.lastIndexOf(temp);
            if(position>=0){
                if(position+(i+1)==len1){

                    if(bestval<i+1) bestval=i+1;
                }
            }else {
                break;
            }
        }
        return bestval;
    }

    public  void readfile() {
        int i=1;
        try {
            File inputFile = new File("src//test//input.dat");
            BufferedReader reader=new BufferedReader(new FileReader(inputFile));
            int c;
            s[i]="";
            reader.readLine();
           
            while ((c = reader.read()) != -1){
                
                if(c == '>')
                {
                    i++;
                    reader.readLine();
                    s[i]="";
                }else {
                   s[i]=s[i]+Character.toString((char)c);
                }
               
            }

            reader.close();
            
        } catch (FileNotFoundException e) {
            System.err.println("FileStreamsTest: " + e);
        } catch (IOException e) {
            System.err.println("FileStreamsTest: " + e);
        }
        no_of_fragments=i;
        int sum=0;
        for(int j=1;j<=i;j++){
            s[j]=s[j].trim();
            System.out.println(s[j].length());
            sum=sum+s[j].length();
            if(j<3) System.out.println(s[j]);
        }
        System.out.println("avg ="+sum/no_of_fragments);
    }

    public  void writefile(int n,int m) {
        try {
            
            File outputFile = new File("src//test//acin9_sanger_output.txt");
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
            

            for(int i=1;i<=n;i++)
            {
                for(int j=1;j<=m;j++)
                {
                   writer.write(Integer.toString(fitarray[i][j])+" ");
                }
                writer.write("\n");
                System.out.println("row"+i+"has been written\n");
                
            }
           writer.flush();
           writer.close();
        } catch (FileNotFoundException e) {
            System.err.println("FileStreamsTest: " + e);
        } catch (IOException e) {
            System.err.println("FileStreamsTest: " + e);
        }
    }
    public  String reverseIt(String source) {
            int i, len = source.length();
            String dest =new String();

            for (i = (len - 1); i >= 0; i--)
                    dest=dest+source.charAt(i);
            return dest;
    }
    public String replace(String s1)
    {
        //System.out.println("coming"+s1);
        char mys[]=s1.toCharArray();
        
        for(int k=0;k<s1.length();k++)
        {
            if(mys[k]=='a' || mys[k]=='A'){
                if(mys[k]=='a') mys[k]='t';
                else mys[k]='T';
            }
            else if(mys[k] =='t' || mys[k]=='T'){
                if(mys[k]=='t'){ mys[k] = 'a';}
                else mys[k]='A';
            }
            else if(mys[k] == 'c' || mys[k]=='C'){
               if(mys[k]=='c') mys[k] = 'g';
               else mys[k]='G';
            }
            else if(mys[k] == 'g'|| mys[k]=='G'){
               if(mys[k]=='g') mys[k] = 'c';
               else mys[k]='C';
            }
        }
        //System.out.println("replace"+String.valueOf(s));
        String reverse= reverseIt(String.valueOf(mys));
        //System.out.println("reverse"+reverse);
        return reverse;
    }
    public void generate_fitarray(int n,int m)
    {
        for(int i=1;i<=n;i++)
        {
            System.out.println("row "+i+"is processing >>>>>>>>>>");
            for(int j=1;j<=m;j++){
                 int sign=0;
                 if(i==j) fitarray[i][j]=0;
                 else if (status[j][i]==-1){
                      fitarray[i][j]=fitarray[j][i];
                 }
                 else {
                       status[i][j]=-1;
                     // i,j
                        fitarray[i][j]=get_longestmatch(s[i], s[j]);
                        String temp=s[j];
                     //i, reverse j
                        int val=get_longestmatch(s[i], replace(temp));
                        if(val>fitarray[i][j]){ fitarray[i][j]=val;sign=1;}
                     //j,i
                        val=get_longestmatch(s[j],s[i]);
                        if(val>fitarray[i][j]){fitarray[i][j]=val;sign=0;}
                        temp=s[i];
                     //j,reverse i
                        val=get_longestmatch(s[j],replace(temp));
                        if(val>fitarray[i][j]){fitarray[i][j]=val;sign=1;}
                    //reverse i, j
                        temp=s[i];
                        val=get_longestmatch(replace(s[i]),s[j]);
                        if(val>fitarray[i][j]){fitarray[i][j]=val;sign=1;}
                     //reverse  i,reverse j
                        temp=s[i];
                        String temp1=s[j];
                        val=get_longestmatch(replace(s[i]),replace(s[j]));
                        if(val>fitarray[i][j]){fitarray[i][j]=val;sign=1;}
                     //reverse j, i
                        temp=s[i];
                        temp1=s[j];
                        val=get_longestmatch(replace(s[j]),s[i]);
                        if(val>fitarray[i][j]){fitarray[i][j]=val;sign=1;}
                     //reverse j, reverse i
                        temp=s[i];
                        temp1=s[j];
                        val=get_longestmatch(replace(s[j]),replace(s[i]));
                        if(val>fitarray[i][j]){fitarray[i][j]=val;sign=1;}
                        if(sign==1) fitarray[i][j]=-fitarray[i][j];

                 }
            }
        }
     }


}
public class Main {
    public static void main(String[] args) {
       
        FileStreamsTest mytest=new FileStreamsTest();
        mytest.readfile();
        System.out.println(mytest.no_of_fragments);
        mytest.generate_fitarray(mytest.no_of_fragments,mytest.no_of_fragments);
        mytest.writefile(mytest.no_of_fragments,mytest.no_of_fragments);
       
    }

}
